<?php
/**
 * SimplePHP Framework (http://simplephp.org)
 *
 * @copyright Copyright (c) 2013 - 2014 Alexandre KOVAC (www.kovacou.fr)
 */

namespace Simple\Parser;

class Annotation {

    /**
     * @var \Simple\Parser\Annotation\Annotated
     */
    protected $class;

    /**
     * @var array
     */
    protected $methods = [];

    /**
     * @var array
     */
    protected $config = [
        'startSymbol'       => '@',
        'endSymbol'         => '[ ]*(?:@|\r\n|\n)',
        'keyPattern'        => '[A-z]+',
        'value.startSymbol' => '(',
        'value.endSymbol'   => ')',
        'inheritance'       => false
    ];

    /**
     * Constructor.
     *
     * @param string Path of the file.
     * @param array  Configuration of annotation engine.
     */
    public function __construct($class, $config = []) {
        $this->config = array_merge($this->config, $config);

        $reflection  = new \ReflectionClass($class);
        $this->class = new Annotation\Annotated($reflection->getName(), $this->parse($reflection->getDocComment()));
        $childMethod = $reflection->getMethods();

        if ($this->config['inheritance']) {
            while ($parent = $reflection->getParentClass()) {
                $this->class = new Annotation\Annotated($parent->getName(), array_merge($this->parse($parent->getDocComment()), $this->class->getAll()));
                $reflection = $parent;
            }
        }
        
        foreach ($childMethod as &$method) {
            $reflectionMethod = new \ReflectionMethod($class, $method->getName());
            $this->methods[]  = new Annotation\Annotated($reflectionMethod->getName(), $this->parse($reflectionMethod->getDocComment()));
        }
    }

    /**
     * Return the class.
     *
     * @return \Simple\Parser\Annotation\Annotated
     */
    public function getClass() {
        return $this->class;
    }

    /**
     * Return methods of the class.
     *
     * @return array(\Simple\Parser\Annotation\Annotated)
     */
    public function getMethods() {
        return $this->methods;
    }

    /**
     * Parsing DocComment.
     *
     * @param string Content of docComment
     */
    protected function parse($docComment) {
        $parameters = [];
        preg_match_all('/'. preg_quote($this->config['startSymbol']) .'(?=(.*)'. $this->config['endSymbol'] .')/U', $docComment, $matches);

        foreach ($matches[1] as $parameter) {
            if (preg_match('/^('. $this->config['keyPattern'] .')'. preg_quote($this->config['value.startSymbol']) .'(.*)'. preg_quote($this->config['value.endSymbol']) .'$/', $parameter, $_matches)) {
                if (true === isset($parameters[$_matches[1]])) {
                    $parameters[$_matches[1]] = array_merge([$parameters[$_matches[1]]], [$this->valueOf($_matches[2])]);
                } else {
                    $parameters[$_matches[1]] = $this->valueOf($_matches[2]);
                }
            }
        }

        return $parameters;
    }

    /**
     * Generate a correct value.
     *
     * @param mixed value
     * @return array
     */
    protected function valueOf($value) {
        $original = $value;

        if (($value = json_decode($value, true)) === null) {
            return $original;
        }

        return $value;
    }

}