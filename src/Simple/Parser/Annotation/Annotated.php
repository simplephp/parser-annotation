<?php
/**
 * SimplePHP Framework (http://simplephp.org)
 *
 * @copyright Copyright (c) 2013 - 2014 Alexandre KOVAC (www.kovacou.fr)
 */

namespace Simple\Parser\Annotation;

class Annotated {
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $annotations;

    /**
     * Constructor.
     *
     * @param string Name of the element
     * @param array  List of annotations
     */
    public function __construct($name, $annotations) {
        $this->name = $name;
        $this->annotations = $annotations;
    }

    /**
     * Return the name.
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Verify if `key` exist.
     *
     * @param string Name of the annotation
     * @return boolean
     */
    public function has($key) {
        return isset($this->annotations[$key]);
    }

    /**
     * Return the value of the key.
     *
     * @param string Name of the annotation
     * @return mixed
     */
    public function valueOf($key) {
        if (true === $this->has($key)) {
            return $this->annotations[$key];
        }
    }

    /**
     * Return all annotations.
     *
     * @return array
     */
    public function getAll() {
        return $this->annotations;
    }
    
}